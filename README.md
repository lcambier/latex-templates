Repository for some latex templates

To correct bibtex/natbib bug (extra space after et al.) in authordate1 style, modifu authordate1.bst, line 1033-1049 using

FUNCTION {format.lab.names}
{ 's :=
  s #1 "{vv~}{ll}" format.name$
  s num.names$ duplicate$
  #2 >
    { pop$ " {\em et~al.}\relax" * }
    { #2 <
        'skip$
        { s #2 "{ff }{vv }{ll}{ jj}" format.name$ "others" =
            { " {\em et~al.}\relax" * }
            { " \& " * s #2 "{vv~}{ll}" format.name$ * }
          if$
        }
      if$
    }
  if$
}

(just the "\ " removed)

The file is on 
/usr/local/texlive/2013/texmf-dist/bibtex/bst/beebe/authordate1.bst
and can be accessed using finder "go to"